#!/usr/bin/python3

import sys

def getNormalizedYear(year):
    return str((float(year) * 99.5) + 1913.5) + '\n'


if __name__ == '__main__':
    f = open(sys.argv[1] + '/geval.vw', 'r', encoding='utf-8')
    if len(sys.argv) > 2:
        s = open(sys.argv[1] + '/out-b-' + sys.argv[2] + '.tsv', "w+")
    else:
        s = open(sys.argv[1] + '/out.tsv', "w+")

    for line in f.readlines():
        s.write(getNormalizedYear(line))

    s.close()
    f.close()

