#!/bin/bash

for i in {2..25}
do
	vw train.vw --cache_file cache_train -b $i -f r_temp
    vw -t --cache_file cache_test -i r_temp -p dev-0/geval.vw dev0.vw
    python3 get_outputs_for_geval.py dev-0 $i
    rm cache_test
    rm dev-0/geval.vw
    rm dev-0/out-b-*
	rm cache_train
	rm r_temp
	rm *.vw
	geval --test-name dev-0 | cut -c 7- > geval_info.tsv
done
