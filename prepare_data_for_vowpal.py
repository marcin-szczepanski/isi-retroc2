#!/usr/bin/python3

import pandas
from nltk import word_tokenize, download

download('punkt')


def prepare_data(Xs, Y, filename, isFile = True):
    if isFile:
        print(Xs)
        Xs = Xs.readlines()
    n = len(Xs)
    f = open(filename, "w+", encoding='utf-8')
    for i in range(n):
        tokens = word_tokenize(Xs[i])
        newX = ''
        if len(Y) > 0:
            newX = str(Y[i]) + ' | '
        else:
            newX = '| '
        for token in tokens:
            if len(token) > 4 and token.isalnum():
                x = token.lower()[:7] + ' '
                newX += x
        print(newX[:-1])
        f.write(newX[:-1] + '\n')
    f.close()


def normalize_Y(YA, YB):
    Y = []

    n = len(YA)

    for i in range(n):
        y = str((((YA[i] + YB[i]) / 2.0) - 1913.5) / 99.5)
        Y.append(y)
        print(y)

    return Y


def main():
    train = pandas.read_csv('train/train.tsv', sep='\t', header=None, names=['beginning_year', 'end_year', 'title', 'symbol', 'text'])
    YA = train['beginning_year']
    YB = train['end_year']
    Y = normalize_Y(YA, YB)

    prepare_data(train['text'], Y, 'train.vw', False)

    dev0 = open('dev-0/in.tsv', 'r', encoding="utf-8")
    prepare_data(dev0, [], 'dev0.vw')

    dev1 = open('dev-1/in.tsv', 'r', encoding="utf-8")
    prepare_data(dev1, [], 'dev1.vw')

    testA = open('test-A/in.tsv', 'r', encoding="utf-8")
    prepare_data(testA, [], 'testA.vw')


main()
