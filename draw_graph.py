#!/usr/bin/python3


import matplotlib.pyplot as plt
import numpy as np
import pandas


def plot_graph(b_values, geval):
    x_min = np.min(b_values) - 1
    x_max = np.max(b_values) + 1
    y_min = 0
    y_max = np.max(geval) + 30
    plt.plot(b_values, geval, 'ro')
    plt.axis([x_min, x_max, y_min, y_max])
    plt.xlabel('Parametr b Vowpal Wabbita')
    plt.ylabel('Wartość RMSE')
    plt.show()


def main():
    geval_data = pandas.read_csv('geval_info.tsv', sep='\t', header=None)
    plot_graph(geval_data[0], geval_data[1])


main()
